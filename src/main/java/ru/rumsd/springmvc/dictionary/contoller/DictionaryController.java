package ru.rumsd.springmvc.dictionary.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rumsd.springmvc.dictionary.model.entity.Dictionary;
import ru.rumsd.springmvc.dictionary.model.entity.Word;
import ru.rumsd.springmvc.dictionary.service.DictionaryService;

import java.util.List;

@RestController
@RequestMapping(value = "dict")
public class DictionaryController {

    private DictionaryService dictionaryService;

    @Autowired
    public DictionaryController(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }


    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Dictionary> getAll() {
        return dictionaryService.getAllDictionary();
    }

    @RequestMapping(method = RequestMethod.GET, value = "{id}", produces = "application/json")
    @ResponseBody
    public Dictionary getById(@PathVariable("id") Long id) {
        return dictionaryService.getDictById(id);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<String> create(@RequestBody Dictionary dictionary) {
        if (dictionaryService.getDictByName(dictionary.getName()) != null) {
            return new ResponseEntity<>("{\"status\": \"Словарь уже существует\"}", HttpStatus.CONFLICT);
        }
        dictionaryService.createDict(dictionary);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{id}", consumes = "application/json")
    public ResponseEntity<String> update(@RequestBody Dictionary dictionary, @PathVariable Long id) {
        if (dictionaryService.getDictById(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        dictionaryService.updateDict(dictionary);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {

        if (dictionaryService.getDictById(id) == null) {
            return new ResponseEntity<>("{\"status\": \"Словарь отсутствует\"}", HttpStatus.NOT_FOUND);
        }
        dictionaryService.deleteDict(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "{id}/words")
    @ResponseBody
    public List<Word> getWordsFromDict(@PathVariable("id") Long id) {
        return dictionaryService.getWordsByDict(dictionaryService.getDictById(id));
    }

    @RequestMapping(method = RequestMethod.GET, value = "check_name/{name}", produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> checkDuplicate(@PathVariable("name") String name) {
        Dictionary dictionary = dictionaryService.getDictByName(name);
        if (dictionary == null) {
            return ResponseEntity.ok("{\"duplicate\": \"false\"}");
        }
        return ResponseEntity.ok("{\"duplicate\": \"true\"}");
    }
}
