package ru.rumsd.springmvc.dictionary.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.rumsd.springmvc.dictionary.service.DictionaryService;

@Controller
public class HomeController {

    private DictionaryService dictionaryService;

    @Autowired
    public HomeController(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showHomePage() {
        return "home";
    }

    @RequestMapping(value = "dictionaries", method = RequestMethod.GET)
    public String dictionaries() {
        return "dict";
    }
}
