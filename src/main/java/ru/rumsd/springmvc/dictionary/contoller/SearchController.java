package ru.rumsd.springmvc.dictionary.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.rumsd.springmvc.dictionary.model.entity.Dictionary;
import ru.rumsd.springmvc.dictionary.model.entity.Word;
import ru.rumsd.springmvc.dictionary.service.DictionaryService;

import java.util.List;

@Controller
@RequestMapping(value = "search")
public class SearchController {

    private DictionaryService dictionaryService;

    @Autowired
    public SearchController(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    @RequestMapping(value = "{dict}/{word}", method = RequestMethod.GET)
    @ResponseBody
    public List<Word> search(@PathVariable("dict") String dict, @PathVariable("word") String word) {
        Dictionary dictionary = dictionaryService.getDictByName(dict);
        List<Word> words = dictionaryService.searchWordInDict(dictionary, word);
        return words;
    }
}
