package ru.rumsd.springmvc.dictionary.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.rumsd.springmvc.dictionary.service.DictionaryService;
import ru.rumsd.springmvc.dictionary.service.Status;
import ru.rumsd.springmvc.dictionary.service.WordDTO;

@Controller
@RequestMapping("word")
public class WordController {

    private DictionaryService dictionaryService;

    @Autowired
    public WordController(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> get(@PathVariable("id") Long id) {
        WordDTO wordDTO = dictionaryService.getWordById(id);
        if (wordDTO == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(wordDTO, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> create(@RequestBody WordDTO wordDTO) {
        Status status = dictionaryService.createWord(wordDTO);
        if (status.isResult()) {
            return new ResponseEntity<>(status, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        Status status = dictionaryService.deleteWordById(id);
        if (status.isResult()) {
            return new ResponseEntity<>(status, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(status, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> update(@RequestBody WordDTO wordDTO) {
        Status status = dictionaryService.updateWord(wordDTO);
        if (status.isResult()) {
            return new ResponseEntity<>(status, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }
    }
}
