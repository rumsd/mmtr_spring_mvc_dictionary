package ru.rumsd.springmvc.dictionary.model.dao;

import ru.rumsd.springmvc.dictionary.model.entity.Dictionary;

import java.util.List;

public interface DictionaryDao {

    Dictionary get(Long id);

    void create(Dictionary dictionary);

    void update(Dictionary dictionary);

    void delete(Long id);

    List<Dictionary> getAll();

    Dictionary getByName(String name);
}
