package ru.rumsd.springmvc.dictionary.model.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.rumsd.springmvc.dictionary.model.entity.Dictionary;

import java.util.List;

@Repository
@Transactional
public class DictionaryDaoImpl implements DictionaryDao {

    private SessionFactory sessionFactory;

    @Autowired
    public DictionaryDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Dictionary get(Long id) {
        return (Dictionary) sessionFactory.getCurrentSession().get(Dictionary.class, id);
    }

    @Override
    public void create(Dictionary dictionary) {
        sessionFactory.getCurrentSession().save(dictionary);
    }

    @Override
    public void update(Dictionary dictionary) {
        sessionFactory.getCurrentSession().saveOrUpdate(dictionary);
    }

    @Override
    public void delete(Long id) {
        Dictionary dictionary = get(id);
        if (dictionary != null) {
            sessionFactory.getCurrentSession().delete(dictionary);
        }
    }

    @Override
    public List<Dictionary> getAll() {
        return sessionFactory.getCurrentSession().createCriteria(Dictionary.class).list();
    }

    @Override
    public Dictionary getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Dictionary where name=:name");
        query.setParameter("name", name);
        return (Dictionary) query.uniqueResult();
    }
}
