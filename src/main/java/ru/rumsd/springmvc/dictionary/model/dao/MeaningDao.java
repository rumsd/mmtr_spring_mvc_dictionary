package ru.rumsd.springmvc.dictionary.model.dao;

import ru.rumsd.springmvc.dictionary.model.entity.Meaning;

public interface MeaningDao {

    Meaning get(Long id);

    void create(Meaning meaning);

    void update(Meaning meaning);

    void delete(Long id);
}
