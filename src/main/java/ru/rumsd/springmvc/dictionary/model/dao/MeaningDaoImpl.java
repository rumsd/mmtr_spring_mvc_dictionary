package ru.rumsd.springmvc.dictionary.model.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.rumsd.springmvc.dictionary.model.entity.Meaning;

@Repository
@Transactional
public class MeaningDaoImpl implements MeaningDao {

    private SessionFactory sessionFactory;

    @Autowired
    public MeaningDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Meaning get(Long id) {
        return (Meaning) sessionFactory.getCurrentSession().get(Meaning.class, id);
    }

    @Override
    public void create(Meaning meaning) {
        sessionFactory.getCurrentSession().save(meaning);
    }

    @Override
    public void update(Meaning meaning) {
        sessionFactory.getCurrentSession().saveOrUpdate(meaning);
    }

    @Override
    public void delete(Long id) {
        Meaning meaning = get(id);
        if (meaning != null) {
            sessionFactory.getCurrentSession().delete(meaning);
        }
    }
}
