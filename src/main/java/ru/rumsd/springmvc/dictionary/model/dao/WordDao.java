package ru.rumsd.springmvc.dictionary.model.dao;

import ru.rumsd.springmvc.dictionary.model.entity.Dictionary;
import ru.rumsd.springmvc.dictionary.model.entity.Word;

import java.util.List;

public interface WordDao {

    Word get(Long id);

    void create(Word word);

    void update(Word newWord);

    void delete(Long id);

    List<Word> getWordsByDict(Dictionary dictionary);

    List<Word> getWordsByPartWord(Dictionary dictionary, String word);

    List<Word> getWordFromDict(Dictionary dictionary, String word);
}
