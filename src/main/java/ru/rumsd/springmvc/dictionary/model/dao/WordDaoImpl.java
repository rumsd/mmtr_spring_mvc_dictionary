package ru.rumsd.springmvc.dictionary.model.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.rumsd.springmvc.dictionary.model.entity.Dictionary;
import ru.rumsd.springmvc.dictionary.model.entity.Word;

import java.util.List;

@Repository
@Transactional
public class WordDaoImpl implements WordDao{

    private SessionFactory sessionFactory;

    @Autowired
    public WordDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Word get(Long id) {
        return (Word) sessionFactory.getCurrentSession().get(Word.class, id);
    }

    @Override
    public void create(Word word) {
        sessionFactory.getCurrentSession().save(word);
    }

    @Override
    public void update(Word newWord) {
        sessionFactory.getCurrentSession().merge(newWord);
    }

    @Override
    public void delete(Long id) {
        Word entity = get(id);
        if (entity != null){
            sessionFactory.getCurrentSession().delete(entity);
        }
    }

    @Override
    public List<Word> getWordsByDict(Dictionary dictionary) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "select w from Word w where w.dictionary=:dictionary";
        Query query = session.createQuery(hql)
                .setParameter("dictionary", dictionary);
        List<Word> words = (List<Word>) query.list();
        return words;
    }

    @Override
    public List<Word> getWordsByPartWord(Dictionary dictionary, String word) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Word " +
                "where dictionary=:dictionary " +
                "AND word LIKE :word");
        query.setParameter("dictionary", dictionary);
        query.setParameter("word", "%" + word + "%");
        List<Word> list = (List<Word>) query.list();
        return list;
    }

    @Override
    public List<Word> getWordFromDict(Dictionary dictionary, String word) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Word " +
                "where dictionary=:dictionary " +
                "AND word LIKE :word");
        query.setParameter("dictionary", dictionary);
        query.setParameter("word", word);
        List<Word> list = (List<Word>) query.list();
        return list;
    }
}
