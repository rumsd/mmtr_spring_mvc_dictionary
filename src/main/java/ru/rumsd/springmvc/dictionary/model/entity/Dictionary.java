package ru.rumsd.springmvc.dictionary.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "dict")
public class Dictionary {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "rule")
    private String rule;
    @Column(name = "description")
    private String description;

    @JsonIgnore
    @OneToMany(mappedBy = "dictionary", cascade = CascadeType.ALL)
    private List<Word> words;

    public Dictionary(String name, String rule, String description) {
        this.name = name;
        this.rule = rule;
        this.description = description;
    }

    public Dictionary() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public List<Word> getWords() {
        if (words == null) {
            words = new ArrayList<>();
        }
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
