package ru.rumsd.springmvc.dictionary.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "meaning")
public class Meaning {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "meaning")
    private String meaning;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "word_id")
    private Word word;

    public Meaning() {
    }

    public Meaning(String meaning, Word word) {
        this.meaning = meaning;
        this.word = word;
    }

    public Meaning(String meaning) {
        this.meaning = meaning;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }
}
