package ru.rumsd.springmvc.dictionary.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "word")
public class Word {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "word")
    private String word;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "dict_id")
    private Dictionary dictionary;

    @OneToMany(mappedBy = "word", fetch = FetchType.EAGER,
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.DETACH, CascadeType.REFRESH, CascadeType.REFRESH}
    )
    private List<Meaning> meanings;

    public Word() {
    }

    public Word(String word, Dictionary dictionary) {
        this.word = word;
        this.dictionary = dictionary;
    }

    public List<Meaning> getMeanings() {
        if (meanings == null) {
            meanings = new ArrayList<>();
        }
        return meanings;
    }

    public void setMeanings(List<Meaning> meanings) {
        this.meanings = meanings;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }
}
