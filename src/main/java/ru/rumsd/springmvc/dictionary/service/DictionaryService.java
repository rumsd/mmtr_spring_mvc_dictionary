package ru.rumsd.springmvc.dictionary.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rumsd.springmvc.dictionary.model.dao.DictionaryDao;
import ru.rumsd.springmvc.dictionary.model.dao.MeaningDao;
import ru.rumsd.springmvc.dictionary.model.dao.WordDao;
import ru.rumsd.springmvc.dictionary.model.entity.Dictionary;
import ru.rumsd.springmvc.dictionary.model.entity.Meaning;
import ru.rumsd.springmvc.dictionary.model.entity.Word;

import java.util.List;

@Service
public class DictionaryService {

    private DictionaryDao dictionaryDao;
    private WordDao wordDao;
    private MeaningDao meaningDao;

    @Autowired
    public DictionaryService(DictionaryDao dictionaryDao, WordDao wordDao, MeaningDao meaningDao) {
        this.dictionaryDao = dictionaryDao;
        this.wordDao = wordDao;
        this.meaningDao = meaningDao;
    }

    //Dictionary methods
    public void createDict(Dictionary dictionary) {
        dictionaryDao.create(dictionary);
    }

    public void deleteDict(Long id) {
        dictionaryDao.delete(id);
    }

    public void updateDict(Dictionary dictionary) {
        dictionaryDao.update(dictionary);
    }

    public Dictionary getDictById(Long id) {
        return dictionaryDao.get(id);
    }

    public Dictionary getDictByName(String name) {
        return dictionaryDao.getByName(name);
    }

    public List<Dictionary> getAllDictionary() {
        return dictionaryDao.getAll();
    }

    //Word methods
    public Status createWord(WordDTO wordDTO) {
        Dictionary dictionary = dictionaryDao.getByName(wordDTO.getDictionary());
        if (dictionary == null) {
            return Status.dictionaryNotFound();
        } else if (wordDao.getWordFromDict(dictionary, wordDTO.getWord()).size() != 0) {
            return Status.duplicateWord();
        } else if (!wordValidator(dictionary, wordDTO.getWord())) {
            return Status.validationError();
        } else {
            Word word = new Word();
            Mapper.toWord(wordDTO, word);
            wordDao.create(word);
            for (Meaning meaning : word.getMeanings()) {
                meaning.setWord(word);
                meaningDao.create(meaning);
            }
            return Status.ok().addMessage(word.getId().toString());
        }

    }

    public Status deleteWordById(Long id) {
        Word word = wordDao.get(id);
        if (word == null) {
            return Status.wordNotFound();
        } else {
            wordDao.delete(id);
            return Status.ok();
        }
    }

    public Status updateWord(WordDTO wordDTO) {
        Dictionary dictionary = dictionaryDao.getByName(wordDTO.getDictionary());
        Word oldWord = wordDao.get(wordDTO.getId());
        Word updatedWord = new Word();
        if (oldWord == null) {
            return Status.wordNotFound();
        } else if (wordDao.getWordFromDict(dictionary, wordDTO.getWord()).size() == 1
                && !wordDTO.getWord().equals(oldWord.getWord())) {
            return Status.duplicateWord();
        } else if (!wordValidator(dictionary, wordDTO.getWord())) {
            return Status.validationError();
        } else {
            List<Meaning> before = wordDao.get(oldWord.getId()).getMeanings();
            before.stream().forEach((m) -> meaningDao.delete(m.getId()));
            Mapper.toWord(wordDTO, updatedWord);
            updatedWord.getMeanings().forEach((m) -> {
                m.setWord(updatedWord);
                meaningDao.create(m);
            });
            wordDao.update(updatedWord);
            return Status.ok().addMessage("Обновлено");
        }
    }

    public WordDTO getWordById(Long id) {
        Word word = wordDao.get(id);
        WordDTO wordDTO = new WordDTO();
        if (word != null) {
            Mapper.toDTO(word, wordDTO);
            return wordDTO;
        } else {
            return null;
        }


    }

    public List<Word> getWordsByDict(Dictionary dictionary) {
        return wordDao.getWordsByDict(dictionary);
    }

    public List<Word> searchWordInDict(Dictionary dictionary, String word) {
        return wordDao.getWordsByPartWord(dictionary, word);
    }

    private boolean wordValidator(Dictionary dictionary, String word) {
        return word.matches(dictionary.getRule());
    }

}
