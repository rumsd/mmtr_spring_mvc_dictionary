package ru.rumsd.springmvc.dictionary.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rumsd.springmvc.dictionary.model.dao.DictionaryDao;
import ru.rumsd.springmvc.dictionary.model.entity.Dictionary;
import ru.rumsd.springmvc.dictionary.model.entity.Meaning;
import ru.rumsd.springmvc.dictionary.model.entity.Word;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class Mapper {

    @Autowired
    static DictionaryDao dictionaryDao;

    static void toDTO(Word word, WordDTO wordDTO) {
        wordDTO.setDictionary(word.getDictionary().getName());
        wordDTO.setId(word.getId());
        wordDTO.setWord(word.getWord());
        List<String> meanings = new ArrayList<>();
        for (Meaning m : word.getMeanings()) {
            String meaning = m.getMeaning();
            meanings.add(meaning);
        }
        wordDTO.setMeanings(meanings);
    }

    static void toWord(WordDTO wordDTO, Word word) {
        Dictionary dictionary = dictionaryDao.getByName(wordDTO.getDictionary());
        word.setId(wordDTO.getId());
        word.setDictionary(dictionary);
        word.setWord(wordDTO.getWord());
        Set<String> meaningSet = new HashSet<>(wordDTO.getMeanings());
        List<Meaning> meanings = new ArrayList<>();
        for (String item : meaningSet) {
            meanings.add(new Meaning(item));
        }
        word.setMeanings(meanings);
    }
}
