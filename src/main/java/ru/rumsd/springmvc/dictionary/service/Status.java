package ru.rumsd.springmvc.dictionary.service;

import java.util.ArrayList;
import java.util.List;

public class Status {
    private static final String OK = "ОК";
    private static final String DUPLICATE_WORD = "Слово уже находится в словаре";
    private static final String DUPLICATE_MEANING = "Значение уже присутствует у слова";
    private static final String VALIDATE_ERROR = "Не соответствует правилу словаря";
    private static final String DICTIONARY_NOT_FOUND = "Словарь не найден";
    private static final String WORD_NOT_FOUND = "Слово не найдено";

    private boolean result;
    private List<String> messages;

    public Status(boolean result) {
        this.result = result;
        this.messages = new ArrayList<>();
    }

    public boolean isResult() {
        return result;
    }

    public List<String> getMessages() {
        return messages;
    }

    public Status addMessage(String message) {
        messages.add(message);
        return this;
    }

    public static Status ok() {
        Status status = new Status(true);
        status.messages.add(OK);
        return status;
    }

    public static Status duplicateWord() {
        Status status = new Status(false);
        status.messages.add(DUPLICATE_WORD);
        return status;
    }

    public static Status duplicateMeaning() {
        Status status = new Status(false);
        status.messages.add(DUPLICATE_MEANING);
        return status;
    }

    public static Status validationError() {
        Status status = new Status(false);
        status.messages.add(VALIDATE_ERROR);
        return status;
    }

    public static Status dictionaryNotFound() {
        Status status = new Status(false);
        status.messages.add(DICTIONARY_NOT_FOUND);
        return status;
    }

    public static Status wordNotFound() {
        Status status = new Status(false);
        status.messages.add(WORD_NOT_FOUND);
        return status;
    }
}
