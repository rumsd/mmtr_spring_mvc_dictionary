//загрузка словарей в список
function renderDictSelect() {
    $.get('dict')
        .done(function (response) {
            var id = null;
            response.forEach(function (dict) {
                if (id === null) {
                    id = dict.id;
                }
                $('#dicts')
                    .append($("<option></option>")
                        .attr("value", dict.id)
                        .text(dict.name));
            });
            loadWordsByDict(id);
        });
}

renderDictSelect();

function renderWordRows(data) {
    var tbody = $('#words tbody');
    tbody.empty();
    for (var i = 0; i < data.length; i++) {
        var w = data[i];
        var m = w.meanings;
        var mList = $('<ul></ul>');
        m.forEach(function (item) {
            mList.append($('<li></li>').text(item.meaning));
        });

        var row = $('<tr></tr>')
            .append($('<td></td>').text(i + 1))
            .append($('<td></td>').text(w.word))
            .append($('<td></td>').append(mList))
            .append($('<td class="display-none"></td>').append(w.id));
        tbody.append(row);
    }
}


function loadWordsByDict(id) {
    $.get('dict/' + id + '/words', function (data) {
        renderWordRows(data);
    });
}


//загрузка словаря при изменении списка словарей
$("#dicts").on("change", function () {
    loadWordsByDict($("#dicts option:selected").val());
});

//поиск в словаре
$("#search").on("keyup", function () {
    var text = $(this).val();
    console.log(text);
    if (!text) {
        var dictID = $("#dicts option:selected").val();
        loadWordsByDict(dictID);
        return;
    }
    var dict = $("#dicts option:selected").text();
    console.log(dict);
    $.get('search/' + dict + "/" + text)
        .done(function (response) {
            renderWordRows(response)
        });
});


// установк строки активной
$("tbody").on("click", "tr", function () {
    $('tr').removeClass('active-row');
    $(this).addClass('active-row');
});

//Открытие окна создания слова
$("#create").click(function () {
    $("#dictId").val($("#dicts option:selected").val());
    $('#mode').val('create');
    $("#saveEditHeader").text('Добавить слово');
    $("#saveEdit").modal('show');
});

//Открытие окна удаления слова
$("#remove").click(function () {
    var activeRow = $(".active-row");
    if (activeRow.length === 0) {
        return;
    }
    var word = activeRow.children().eq(1).text();
    $('#deletedWordId').val($('.active-row .display-none').text());
    $("#deleted-word").text(word);
    $("#removeModal").modal('show');
});

//Удаление слова
$("#deleteModalButton").click(function () {
    var deletedWordId = $('.active-row .display-none').text();
    $.ajax({
        url: 'word/' + deletedWordId,
        type: 'DELETE'
    }).done(function (response) {
        var dictID = $("#dicts option:selected").val();
        loadWordsByDict(dictID);
        $("#removeModal").modal('hide');
    });
});


//добавить новое значение
$("#addMeaning").click(function () {
    var meaningBlock = $('<div class="input-group"></div>');
    meaningBlock
        .append($('<input type="text" class="form-control" name="meaning" placeholder="Введите значение">'))
        .append('<span class="delete-input btn btn-danger fa fa-trash"></span>');
    $(this).before(meaningBlock);
});

//закрытие модального окна
$("#saveEdit")
    .on("click", ".delete-input", function () {
        $(this).parent().remove();
    })
    .on('hidden.bs.modal', function () {
        $('.delete-input').each(function () {
            $(this).parent().remove();
        });
        $("#errors").empty();
        $('#word').val('');
        $('#mode').val('');
        $('input[name="meaning"]').val('');
    });

//Открытие окна изменения
$('#change').click(function () {
    $('#mode').val('edit');
    var activeRow = $(".active-row");
    if (activeRow.length === 0) {
        return;
    }
    $("#dictId").val($("#dicts option:selected").val());
    $("#saveEditHeader").text("Изменить слово");
    var word = activeRow.children().eq(1).text();
    $('#wordId').val($('.active-row .display-none').text());
    var meanings = [];
    var tmp = $('.active-row > td:nth-child(3) li').toArray();
    tmp.forEach(function (li) {
        meanings.push($(li).text());
    });
    $('#word').val(word);
    $('input[name="meaning"]').val(meanings[0]);
    for (var i = 1; i < meanings.length; i++) {
        var meaningBlock = $('<div class="input-group"></div>');
        meaningBlock
            .append($('<input type="text" class="form-control" name="meaning" placeholder="Введите значение" value="' + meanings[i] + '">'))
            .append('<span class="delete-input btn btn-danger fa fa-trash"></span>');
        $('#addMeaning').before(meaningBlock);
    }
    $("#saveEdit").modal('show');
});

function showErrors(response) {
    console.log(response.responseJSON);
    var messages = response.responseJSON['messages'];
    var errors = $('#errors');
    errors.empty();
    for (var i = 0; i < messages.length; i++) {
        var p = '<p class="bg-danger text-white">' + messages + '</p>';
        errors.append(p);
    }
    errors.removeClass("display-none");
}

function hideModalAndLoadWords() {
    var dictID = $("#dicts option:selected").val();
    loadWordsByDict(dictID);
    $("#saveEdit").modal('hide');
}

function createWord(json) {
    var wordJson = JSON.stringify(json);
    console.log(wordJson);
    $.ajax('word', {
        type: "post",
        contentType: "application/json; charset=utf-8",
        data: wordJson
    }).done(function (response) {
        hideModalAndLoadWords();
    }).fail(function (response) {
        showErrors(response);
    });
}

function editWord(json) {
    var wordJson = JSON.stringify(json);
    console.log(wordJson);
    $.ajax('word', {
        type: "put",
        contentType: "application/json; charset=utf-8",
        data: wordJson
    }).done(function (response) {
        hideModalAndLoadWords();
    }).fail(function (response) {
        showErrors(response);
    });
}

$('#saveModalButton').click(function () {
    var mode = $('#mode').val();
    var dict = $('#dicts option:selected').text();
    var word = $('#word').val();
    var id = $('#wordId').val();
    var meanings = [];
    var tmp = $('input[name="meaning"]');
    for (var i = 0; i < tmp.length; i++) {
        meanings.push(tmp[i].value);
    }

    switch (mode) {
        case "create": {
            createWord({"dictionary": dict, "word": word, "meanings": meanings});
            break;
        }
        case "edit": {
            editWord({"dictionary": dict, "id": id, "word": word, "meanings": meanings});
            break;
        }
        default: {
            alert("Error!");
        }

    }

});


