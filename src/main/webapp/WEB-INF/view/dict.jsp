<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Список словарей</title>
</head>
<body>
<h1>Доступные словари</h1>
<div>
    <ul>
        <li>
            <button>Добавить</button>
        </li>
        <li>
            <button>Изменить</button>
        </li>
        <li>
            <button>Удалить</button>
        </li>
    </ul>
    <table>
        <tr>
            <th></th>
            <th>#</th>
            <th>Название</th>
            <th>Правило</th>
            <th>Описание</th>
        </tr>
        <%--<c:set var="count" value="0"/>--%>
        <%--<c:forEach var="dict" items="${dicts}">--%>
        <%--<c:set var="count" value="${count+1}"/>--%>
        <%--<tr>--%>
        <%--<td><input type="checkbox" name="row_id" value="${dict.id}"></td>--%>
        <%--<td>${count}</td>--%>
        <%--<td>${dict.name}</td>--%>
        <%--<td>${dict.rule}</td>--%>
        <%--<td>${dict.description}</td>--%>

        <%--</tr>--%>
        <%--</c:forEach>--%>
    </table>
</div>
</body>
</html>
