<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/main.css">
</head>
<body>
<main class="container">
    <header>
        <div class="dict-header">
            <h1>Словарь</h1>
            <div>
                <select class="form-control" name="dicts" id="dicts">
                </select>
            </div>
        </div>
    </header>
    <section class="control-panel">
        <div class="form-inline">
            <button class="btn btn-secondary mr-sm-2" id="create">Добавить</button>
            <button class="btn btn-secondary mr-sm-2" id="change">Изменить</button>
            <button class="btn btn-secondary mr-sm-2" id="remove">Удалить</button>
            <input class="form-control" type="text" placeholder="Поиск" id="search">
        </div>

    </section>
    <section class="row">
        <div class="col-md-12">
            <table id="words" class="table table-bordered table-hover">
                <thead class="thead-light">
                <tr class="table-header">
                    <th>#</th>
                    <th>Слово</th>
                    <th>Значения</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </section>
</main>
<!-- Модальное окно редактирования/создания -->
<div id="saveEdit" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="saveEditHeader"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="" id="saveEditForm">
                    <input type="hidden" id="mode" name="mode" value="">
                    <input type="hidden" id="dictId" name="dictId" value="">
                    <input type="hidden" id="wordId" name="wordId" value="">
                    <div class="form-group display-none" id="errors">

                    </div>
                    <div class="form-group">
                        <label>Слово</label>
                        <input type="text" class="form-control" id="word" placeholder="Введите слово">
                    </div>
                    <div class="meanings form-group">
                        <label>Значения:</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="meaning" placeholder="Введите значение">
                        </div>
                        <span class="btn btn-success fa fa-plus" id="addMeaning"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="saveModalButton">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>

<!-- Модальное окно удаления -->
<div id="removeModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Удалить слово <span id="deleted-word"></span>?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <input type="hidden" name="deletedWordId" id="deletedWordId" value="">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="deleteModalButton">Удалить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="application/javascript"
        src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script type="application/javascript" src="${pageContext.request.contextPath}/scripts/home.js"></script>
<script type="application/javascript"></script>
</body>
</html>
